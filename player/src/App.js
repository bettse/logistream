import React from 'react';

import { isFirefox } from "react-device-detect";
import { When } from "react-if";

import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Jumbotron from 'react-bootstrap/Jumbotron'

import ReactHlsPlayer from 'react-hls-player'

function App() {
  return (
    <>
      <Navbar>
        <Navbar.Brand>
          <a href="/">LogiStream</a>
        </Navbar.Brand>
      </Navbar>
      <Container>
        <Row>
          <Col>
            <Jumbotron>
              <When condition={isFirefox}><h3>{"Doesn't seem to work in Firefox, maybe try Chrome or iOS?"}</h3></When>
              <ReactHlsPlayer url='/stream/stream.m3u8' controls hlsConfig={{debug: true}} />
            </Jumbotron>
          </Col>
        </Row>
      </Container>
      <footer className="page-footer font-small pt-5 mt-5 mx-auto">
        <Container fluid className="text-center">
        </Container>
      </footer>
    </>
  );
}

export default App;
