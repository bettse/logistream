const crypto = require('crypto');
const fs = require('fs')
const { spawn } = require('child_process');
const debug = require('debug')('LogiStream')
const vlclog = require('debug')('VLC')
const {IPDiscovery, HttpClient, HttpConstants, TLV} = require('hap-controller');
const dgram = require('dgram')
const is_rtp = require('is-rtp')
const { SrtcpSession, SrtpSession, RtcpRrPacket, RtcpReceiverInfo, RtpPacket, RtcpPacketConverter } = require('werift-rtp')
const { UdpTransport, createUdpTransport } = require('werift-rtp/lib/transport')
const { RtcpSourceDescriptionPacket, SourceDescriptionChunk, SourceDescriptionItem } = require('werift-rtp/lib/rtcp/sdes')
const HttpServer = require('http-server')

const camera_tlv = require('./camera_tlv')

const { SetupEndpointsTypes, SessionControlCommand, AddressTypes, IPAddressVersion, SRTPParametersTypes, SRTPCryptoSuites } = camera_tlv;
const { StreamingStatusTypes, StreamingStatus } = camera_tlv
const { SupportedVideoStreamConfigurationTypes, VideoCodecConfigurationTypes, VideoCodecParametersTypes, VideoAttributesTypes, VideoCodecType } = camera_tlv;
const { SetupEndpointsResponseTypes, SetupEndpointsStatus } = camera_tlv
const { SelectedRTPStreamConfigurationTypes, SessionControlTypes, SelectedVideoParametersTypes } = camera_tlv;
const { H264Profile, H264Level, VideoCodecPacketizationMode, VideoCodecCVO, VideoRTPParametersTypes } = camera_tlv;
const { SupportedRTPConfigurationTypes, SupportedAudioStreamConfigurationTypes, AudioCodecConfigurationTypes, AudioCodecParametersTypes } = camera_tlv;
const { AudioBitrate, AudioSamplerate, AudioRTPParametersTypes, SelectedAudioParametersTypes, AudioCodecTypes } = camera_tlv

const pairingFile = './device.json'
let pairingData = null;
if (fs.existsSync(pairingFile)) {
  pairingData = require(pairingFile)
}

const pin = '861-30-040'
const sessionId = crypto.randomBytes(16); // Buffer.from('91c4827f5b4cc89a55ed1aaf9c864303', 'hex')
let startupRTCP = null;
let sendRR = null;
let highestSequence = 0

const audioPort = 50022;
const videoPort = 50024;

const localVideoPort = 60024
const localControlPort = 60025

const vlc_args = ['-I', 'rc', './video.sdp', '--sout', '#std{access=livehttp{seglen=5,delsegs=true,numsegs=5,index=./public/stream.m3u8,index-url=./stream-########.ts},mux=ts{use-key-frames},dst=./public/stream-########.ts}']

const SSRC = {
  local: {
    video: crypto.randomBytes(4), //Buffer.from('01020304', 'hex'),
    audio: crypto.randomBytes(4), //Buffer.from('00000002', 'hex'),
  },
  remote: {
  }
}

// for video TODO: add nesting for audio/video
const keys = {
  audio: {
    localMasterKey: Buffer.from('de4d12acaca276b011a2b71cd8c6bc1f', 'hex'),
    localMasterSalt: Buffer.from('bde343df785d9fe1b66cd325404f', 'hex'),
  },
  video: {
    localMasterKey: Buffer.from('de4d12acaca276b011a2b71cd8c6bc1f', 'hex'),
    localMasterSalt: Buffer.from('bde343df785d9fe1b66cd325404f', 'hex'),
  },
}

const audioSocket = dgram.createSocket('udp4')
audioSocket.bind(audioPort)
const videoSocket = dgram.createSocket('udp4')
videoSocket.bind(videoPort)

let videoTransport

const localVideoSocket = dgram.createSocket('udp4')
const localControlSocket = dgram.createSocket('udp4')

let srtcpSession = null;
let srtpSession = null;

const httpServer = HttpServer.createServer({
  cors: true,
  cache: -1,
})
httpServer.listen(8080)


const vlc_process = spawn('vlc', vlc_args)
vlc_process.stdout.on('data', (data) => {
  vlclog('stdout:', data.toString());
});
vlc_process.stderr.on('data', (data) => {
  vlclog('stderr:', data.toString());
});
vlc_process.on('close', (code) => {
  console.log(`vlc_process exited with code ${code}`);
});

videoSocket.on('message', async (packet) => {
  if (startupRTCP) {
    clearInterval(startupRTCP)
    startupRTCP = null;
  }
  if (srtpSession === null) {
    if (keys.video) {
      srtpSession = new SrtpSession({profile: 0x0001, keys: keys.video});
    } else {
      console.log("Attempting to setup SRTP Session without having keys")
      return
    }
  }
  const isRtp = is_rtp(packet);

  try {
    if (isRtp) {
      const clear = srtpSession.decrypt(packet)
      const rtp = RtpPacket.deSerialize(clear)
      highestSequence = Math.max(highestSequence, rtp.header.sequenceNumber)
      localVideoSocket.send(clear, localVideoPort, '127.0.0.1', (err, size) => {
        if (err) {
          console.log(err)
        }
      })
      //await sendVideo(packet, localVideoPort, '127.0.0.1')
      // console.log(rtp)
    } else {
      const clear = srtcpSession.decrypt(packet)
      const rtcp = RtcpPacketConverter.deSerialize(clear)
      console.log({rtcp})
      localVideoSocket.send(rtcp.serialize(), localControlPort, '127.0.0.1', console.log)
    }
  } catch(e) {
    console.log('videoSocket caught', e)
  }
})

audioSocket.on('message', (packet) => {
  // console.log('audioSocket message')
  if (startupRTCP) {
    clearInterval(startupRTCP)
    startupRTCP = null;
  }
})

function startRR() {
  console.log('startRR')
  if (sendRR) { return }
  sendRR = setInterval(() => {
    // console.log('sendRR')
    const rr = new RtcpRrPacket({
      ssrc: SSRC.local.video.readUInt32BE(),
      reports: [
        new RtcpReceiverInfo({
          ssrc: SSRC.remote.video.readUInt32BE(),
          packetsLost: 1,
          //fractionLost,
          highestSequence,
          //jitter,
          //lsr,
          //dlsr,
        })
      ],
    }).serialize();

    videoTransport.send(srtcpSession.encrypt(rr))
  }, 3000)
}

async function onAccessoryEvent(event) {
  const { characteristics } = event;
  characteristics.forEach(async (characteristic) => {
    const { iid, status, value } = characteristic;
    if (status) {
      const error = HttpConstants.StatusCodes[status]
      throw error
    }
    if (iid === 10 || iid === 17) {
      const streamingStatus = await parseStreamingStatus(event)
      if (streamingStatus === StreamingStatus.IN_USE) { // in this context, 'ready'
        await sendStartupRTCP()
      } else if (streamingStatus === StreamingStatus.UNAVAILABLE) {
        clearInterval(sendRR);
        sendRR = null;
      }
    }
    if (iid === 41) {
      debug(`motion ${value}`)
    }
  })
}

async function sendStartupRTCP() {
  if (srtcpSession === null) {
    srtcpSession = new SrtcpSession({profile: 0x0001, keys: keys.video});
  }
  startRR()

  const rr = new RtcpRrPacket({
    ssrc: SSRC.local.video.readUInt32BE(),
    reports: [
      new RtcpReceiverInfo({ ssrc: SSRC.remote.video.readUInt32BE(), packetsLost: 1 })
    ],
  }).serialize();

  const sdes = new RtcpSourceDescriptionPacket({
    chunks: [
      new SourceDescriptionChunk({
        source: SSRC.local.video.readUInt32BE(),
        items: [
          new SourceDescriptionItem({
            type: 1,
            text: ['root', crypto.randomBytes(4).toString('hex'), '@', '10.0.1.4'].join('')
          })
        ]
      })
    ]
  }).serialize()

  // TODO: make this a real class
  const tlfm = Buffer.from(`83cd0004${SSRC.local.video.toString('hex')}00000000${SSRC.remote.video.toString('hex')}1a47fc28`, 'hex')
  const packet = Buffer.concat([rr, sdes, tlfm])

  let count = 0;
  startupRTCP = setInterval(() => {
    const enc = srtcpSession.encrypt(packet)
    videoTransport.send(enc)
    count = count + 1;
    if (count > 20) {
      clearInterval(startupRTCP);
      console.log('done sending RTCP')
      startupRTCP = null;
    }
  }, 500)
}

const ipDiscovery = new IPDiscovery();
ipDiscovery.on('serviceUp', async (service) => {
  try {
    const { address, name, id, port } = service;
    console.log({name})
    if (name === 'Logi Circle') {
      if (pairingData) {
        const ipClient = new HttpClient(id, address, port, pairingData);

        videoTransport = new UdpTransport(videoSocket, {
          address,
          family: 'IPv4',
          port: videoPort,
        })

        //await ipClient.removePairing(pairingData.AccessoryPairingID);

        ipClient.on('event', onAccessoryEvent)

        await ipClient.subscribeCharacteristics(['1.17', '1.41'])

        // await getSupportedRtpConfiguration(ipClient)
        await getStreamingStatus(ipClient)

        //TODO: error checking helper
        await sendSetupEndpoints(ipClient)
        await getSetupEndpoints(ipClient)

        const rtpStreamConfigResponse = await sendRTPStreamConfig(ipClient)
        await parseRTPStreamConfigResponse(rtpStreamConfigResponse)
      } else {
        const ipClient = new HttpClient(id, address, port);
        await ipClient.pairSetup(pin)
        fs.writeFileSync(pairingFile, JSON.stringify(ipClient.getLongTermData(), null, 2))
        process.exit()
      }
    }
  } catch(e) {
    console.log('caught', e)
    process.exit()
  }
});

async function getAccessories(ipClient) {
  const {accessories} = await ipClient.getAccessories()
  const [accessory] = accessories
  const { services } = accessory
  services.forEach((service) => {
    if (service.type === '110') {
      console.log(service)
    }
  })
}

async function getStreamingStatus(ipClient) {
  const response = await ipClient.getCharacteristics(['1.17'])
  return parseStreamingStatus(response)
}

async function parseStreamingStatus(response) {
  const { characteristics } = response
  const [characteristic] = characteristics
  //console.log('parseStreamingStatus', characteristic)
  const { status, value } = characteristic
  if (status) {
    const error = HttpConstants.StatusCodes[status]
    throw error
  }
  const buffer = Buffer.from(value, 'base64')
  const decoded = TLV.decodeBuffer(buffer)
  //console.log('parseStreamingStatus', decoded)
  const streamingStatus = decoded.get(StreamingStatusTypes.STATUS).readUInt8();
  debug({streamingStatus})
  return streamingStatus
}

async function getSupportedRtpConfiguration(ipClient) {
  const response = await ipClient.getCharacteristics(['1.20'])
  const { characteristics } = response
  const [characteristic] = characteristics
  //console.log('getSupportedRtpConfiguration', characteristic)
  const { status, value } = characteristic
  if (status) {
    const error = HttpConstants.StatusCodes[status]
    throw error
  }
  const buffer = Buffer.from(value, 'base64')
  const decoded = TLV.decodeBuffer(buffer)
  //console.log('getSupportedRtpConfiguration', decoded)

  const srtpCryptoSuite = decoded.get(SupportedRTPConfigurationTypes.SRTP_CRYPTO_SUITE).readUInt8()

  //console.log('getSupportedRtpConfiguration', {srtpCryptoSuite})
}

async function getSetupEndpoints(ipClient) {
  const response = await ipClient.getCharacteristics(['1.21'])
  return parseSetupEndpoint(response)
}

async function parseSetupEndpoint(response) {
  const { characteristics } = response
  const [characteristic] = characteristics
  // console.log('parseSetupEndpoint', characteristic)
  const { status, value } = characteristic
  if (status) {
    const error = HttpConstants.StatusCodes[status]
    throw error
  }
  if (value) {
    const buffer = Buffer.from(value, 'base64')
    const decoded = TLV.decodeBuffer(buffer)
    // console.log('parseSetupEndpoint', decoded)

    const sessionId = decoded.get(SetupEndpointsResponseTypes.SESSION_ID).toString('hex')
    const endpointStatus = decoded.get(SetupEndpointsResponseTypes.STATUS)

    const accessoryAddress = TLV.decodeBuffer(decoded.get(SetupEndpointsResponseTypes.ACCESSORY_ADDRESS))
    const addressVersion = accessoryAddress.get(AddressTypes.ADDRESS_VERSION).readUInt8()
    const address = accessoryAddress.get(AddressTypes.ADDRESS).toString()
    const videoRtpPort = accessoryAddress.get(AddressTypes.VIDEO_RTP_PORT).readUInt16LE()
    const audioRtpPort = accessoryAddress.get(AddressTypes.AUDIO_RTP_PORT).readUInt16LE()

    const videoSrtpParameters = TLV.decodeBuffer(decoded.get(SetupEndpointsResponseTypes.VIDEO_SRTP_PARAMETERS))

    const videoSrtpCryptoSuite = videoSrtpParameters.get(SRTPParametersTypes.SRTP_CRYPTO_SUITE).readUInt8()
    if (videoSrtpCryptoSuite === SRTPCryptoSuites.AES_CM_128_HMAC_SHA1_80) {
      const videoMasterKey = videoSrtpParameters.get(SRTPParametersTypes.MASTER_KEY)
      const videoMasterSalt = videoSrtpParameters.get(SRTPParametersTypes.MASTER_SALT)
      //console.log('save video master key/salt', {videoMasterKey, videoMasterSalt})
      keys.video = {
        ...keys.video,
        remoteMasterKey: videoMasterKey,
        remoteMasterSalt: videoMasterSalt,
      }
    }

    const audioSrtpParameters = TLV.decodeBuffer(decoded.get(SetupEndpointsResponseTypes.AUDIO_SRTP_PARAMETERS))

    const audioSrtpCryptoSuite = audioSrtpParameters.get(SRTPParametersTypes.SRTP_CRYPTO_SUITE).readUInt8()
    if (audioSrtpCryptoSuite === SRTPCryptoSuites.AES_CM_128_HMAC_SHA1_80) {
      const audioMasterKey = audioSrtpParameters.get(SRTPParametersTypes.MASTER_KEY)
      const audioMasterSalt = audioSrtpParameters.get(SRTPParametersTypes.MASTER_SALT)
      //console.log('save audio master key/salt', {audioMasterKey, audioMasterSalt})
      keys.audio = {
        ...keys.audio,
        remoteMasterKey: audioMasterKey,
        remoteMasterSalt: audioMasterSalt,
      }
    }

    const videoSsrc = decoded.get(SetupEndpointsResponseTypes.VIDEO_SSRC)
    const audioSsrc = decoded.get(SetupEndpointsResponseTypes.AUDIO_SSRC)
    if (videoSsrc && audioSsrc){
      // console.log('save audio and video ssrc', {videoSsrc, audioSsrc})
      SSRC.remote = {
        video: Buffer.from(videoSsrc).swap32(),
        audio: Buffer.from(audioSsrc).swap32(),
      }
    }

    // console.log('parseSetupEndpoint', {sessionId, status, accessoryAddress: {addressVersion, address, videoRtpPort, audioRtpPort}, videoSrtpParameters, audioSrtpParameters, videoSsrc, audioSsrc})
    return {sessionId, status, accessoryAddress: {addressVersion, address, videoRtpPort, audioRtpPort}, videoSrtpParameters, audioSrtpParameters, videoSsrc, audioSsrc}
  }
}

async function getSupportedVideoStreamConfiguration(ipClient) {
  const response = await ipClient.getCharacteristics(['1.18'])
  const { characteristics } = response
  const [characteristic] = characteristics
  // console.log('getSupportedVideoStreamConfiguration', characteristic)
  const { status, value } = characteristic
  if (status) {
    const error = HttpConstants.StatusCodes[status]
    throw error
  }
  const buffer = Buffer.from(value, 'base64')
  const decoded = TLV.decodeBuffer(buffer)
  //console.log('getSupportedVideoStreamConfiguration', {decoded})
  const videoCodecConfigBuffer = decoded.get(SupportedVideoStreamConfigurationTypes.VIDEO_CODEC_CONFIGURATION)
  const videoCodecConfig = TLV.decodeBuffer(videoCodecConfigBuffer)
  const codecType = videoCodecConfig.get(VideoCodecConfigurationTypes.CODEC_TYPE);
  const codecParameters = TLV.decodeBuffer(videoCodecConfig.get(VideoCodecConfigurationTypes.CODEC_PARAMETERS));
  const profileId = codecParameters.get(VideoCodecParametersTypes.PROFILE_ID).readUInt8()
  const level = codecParameters.get(VideoCodecParametersTypes.LEVEL).readUInt8()
  const packetizationMode = codecParameters.get(VideoCodecParametersTypes.PACKETIZATION_MODE).readUInt8()
  const cvoEnabled = codecParameters.get(VideoCodecParametersTypes.CVO_ENABLED).readUInt8()

  const attrs = videoCodecConfig.get(VideoCodecConfigurationTypes.ATTRIBUTES);
  const attributes = attrs.map((attr) => {
    const attribute = TLV.decodeBuffer(attr)
    const imageWidth = attribute.get(VideoAttributesTypes.IMAGE_WIDTH).readUInt16LE();
    const imageHeight = attribute.get(VideoAttributesTypes.IMAGE_HEIGHT).readUInt16LE();
    const frameRate = attribute.get(VideoAttributesTypes.FRAME_RATE).readUInt8();
    return {imageWidth, imageHeight, frameRate}
  })
  //console.log({codecType, codecParameters: {profileId, level, packetizationMode, cvoEnabled}, attributes})
}

async function getSupportedAudioStreamConfiguration(ipClient) {
  const response = await ipClient.getCharacteristics(['1.19'])
  const { characteristics } = response
  const [characteristic] = characteristics
  //console.log('getSupportedAudioStreamConfiguration', characteristic)
  const { status, value } = characteristic
  if (status) {
    const error = HttpConstants.StatusCodes[status]
    throw error
  }
  const buffer = Buffer.from(value, 'base64')
  const decoded = TLV.decodeBuffer(buffer)
  //console.log('getSupportedAudioStreamConfiguration', {decoded})
  const confortNoiseSupport = decoded.get(SupportedAudioStreamConfigurationTypes.COMFORT_NOISE_SUPPORT)
  const audioCodecConfigBuffer = decoded.get(SupportedAudioStreamConfigurationTypes.AUDIO_CODEC_CONFIGURATION)
  const audioCodecConfig = TLV.decodeBuffer(audioCodecConfigBuffer)


  const codecType = audioCodecConfig.get(AudioCodecConfigurationTypes.CODEC_TYPE);
  const codecParameters = TLV.decodeBuffer(audioCodecConfig.get(AudioCodecConfigurationTypes.CODEC_PARAMETERS));
  const channel = codecParameters.get(AudioCodecParametersTypes.CHANNEL)
  const bitRate = codecParameters.get(AudioCodecParametersTypes.BIT_RATE)
  const sampleRate = codecParameters.get(AudioCodecParametersTypes.SAMPLE_RATE)
  const packetTime = codecParameters.get(AudioCodecParametersTypes.PACKET_TIME)

  //console.log({codecType, codecParameters: {channel, bitRate, sampleRate, packetTime}})
}

async function sendSetupEndpoints(ipClient) {
  const vRtpPort = Buffer.alloc(2)
  vRtpPort.writeUInt16LE(videoPort)
  const aRtpPort = Buffer.alloc(2)
  aRtpPort.writeUInt16LE(audioPort)

  const controllerAddress = new Map();
  controllerAddress.set(AddressTypes.ADDRESS_VERSION, Buffer.from([IPAddressVersion.IPV4]));
  controllerAddress.set(AddressTypes.ADDRESS, Buffer.from('10.0.1.4'));
  controllerAddress.set(AddressTypes.VIDEO_RTP_PORT, vRtpPort)
  controllerAddress.set(AddressTypes.AUDIO_RTP_PORT, aRtpPort)

  const videoSrtpParameters = new Map();
  videoSrtpParameters.set(SRTPParametersTypes.SRTP_CRYPTO_SUITE, Buffer.from([SRTPCryptoSuites.AES_CM_128_HMAC_SHA1_80]))
  videoSrtpParameters.set(SRTPParametersTypes.MASTER_KEY, keys.video.localMasterKey)
  videoSrtpParameters.set(SRTPParametersTypes.MASTER_SALT, keys.video.localMasterSalt)

  const audioSrtpParameters = new Map();
  audioSrtpParameters.set(SRTPParametersTypes.SRTP_CRYPTO_SUITE, Buffer.from([SRTPCryptoSuites.AES_CM_128_HMAC_SHA1_80]))
  audioSrtpParameters.set(SRTPParametersTypes.MASTER_KEY, keys.audio.localMasterKey)
  audioSrtpParameters.set(SRTPParametersTypes.MASTER_SALT, keys.audio.localMasterSalt)

  const setup = new Map();
  setup.set(SetupEndpointsTypes.SESSION_ID, sessionId)
  setup.set(SetupEndpointsTypes.CONTROLLER_ADDRESS, TLV.encodeObject(controllerAddress))
  setup.set(SetupEndpointsTypes.VIDEO_SRTP_PARAMETERS, TLV.encodeObject(videoSrtpParameters))
  setup.set(SetupEndpointsTypes.AUDIO_SRTP_PARAMETERS, TLV.encodeObject(audioSrtpParameters))

  return await ipClient.setCharacteristics({
    '1.21': TLV.encodeObject(setup).toString('base64')
  })
}

async function sendRTPStreamConfig(ipClient) {
  const sessionControl = new Map();
  sessionControl.set(SessionControlTypes.SESSION_IDENTIFIER, sessionId)
  sessionControl.set(SessionControlTypes.COMMAND, Buffer.from([SessionControlCommand.START_SESSION]))

  const videoCodecParameters = new Map();
  videoCodecParameters.set(VideoCodecParametersTypes.PROFILE_ID, Buffer.from([H264Profile.MAIN]))
  videoCodecParameters.set(VideoCodecParametersTypes.LEVEL, Buffer.from([H264Level.LEVEL4_0]))
  videoCodecParameters.set(VideoCodecParametersTypes.PACKETIZATION_MODE, Buffer.from([VideoCodecPacketizationMode.NON_INTERLEAVED]))
  // videoCodecParameters.set(VideoCodecParametersTypes.CVO_ENABLED, Buffer.from([0]))
  // videoCodecParameters.set(VideoCodecParametersTypes.CVO_ID, Buffer.from([1]))

  const videoAttributes = new Map();
  videoAttributes.set(VideoAttributesTypes.IMAGE_WIDTH, Buffer.from('0005', 'hex'))
  videoAttributes.set(VideoAttributesTypes.IMAGE_HEIGHT, Buffer.from('d002', 'hex'))
  videoAttributes.set(VideoAttributesTypes.FRAME_RATE, Buffer.from([30]))

  const videoRtpParameters = new Map();
  videoRtpParameters.set(VideoRTPParametersTypes.PAYLOAD_TYPE, Buffer.from([0x63]))
  videoRtpParameters.set(VideoRTPParametersTypes.SYNCHRONIZATION_SOURCE, Buffer.from(SSRC.local.video).swap32())
  videoRtpParameters.set(VideoRTPParametersTypes.MAX_BIT_RATE, Buffer.from('2b01', 'hex'))
  videoRtpParameters.set(VideoRTPParametersTypes.MIN_RTCP_INTERVAL, Buffer.from('0000003f', 'hex'))
  videoRtpParameters.set(VideoRTPParametersTypes.MAX_MTU, Buffer.from('6205', 'hex'))
  // console.log('sendRTPStreamConfig', {videoRtpParameters})

  const videoParameters = new Map();
  videoParameters.set(SelectedVideoParametersTypes.CODEC_TYPE, Buffer.from([VideoCodecType.H264]))
  videoParameters.set(SelectedVideoParametersTypes.CODEC_PARAMETERS, TLV.encodeObject(videoCodecParameters))
  videoParameters.set(SelectedVideoParametersTypes.ATTRIBUTES, TLV.encodeObject(videoAttributes))
  videoParameters.set(SelectedVideoParametersTypes.RTP_PARAMETERS, TLV.encodeObject(videoRtpParameters))

  const audioCodecParameters = new Map();
  audioCodecParameters.set(AudioCodecParametersTypes.CHANNEL, Buffer.from([1]))
  audioCodecParameters.set(AudioCodecParametersTypes.BIT_RATE, Buffer.from([AudioBitrate.VARIABLE]))
  audioCodecParameters.set(AudioCodecParametersTypes.SAMPLE_RATE, Buffer.from([AudioSamplerate.KHZ_16]))
  audioCodecParameters.set(AudioCodecParametersTypes.PACKET_TIME, Buffer.from([20]))
  //console.log({audioCodecParameters})

  const audioRtpParameters = new Map();
  audioRtpParameters.set(AudioRTPParametersTypes.PAYLOAD_TYPE, Buffer.from([0x6E]))
  audioRtpParameters.set(AudioRTPParametersTypes.SYNCHRONIZATION_SOURCE, Buffer.from(SSRC.local.audio).swap32())
  audioRtpParameters.set(AudioRTPParametersTypes.MAX_BIT_RATE, Buffer.from('1800', 'hex'))
  audioRtpParameters.set(AudioRTPParametersTypes.MIN_RTCP_INTERVAL, Buffer.from('0000a040', 'hex'))
  // audioRtpParameters.set(AudioRTPParametersTypes.COMFORT_NOISE_PAYLOAD_TYPE, Buffer.from([0x0d]))
  // console.log('sendRTPStreamConfig', {audioRtpParameters})

  const audioParameters = new Map();
  audioParameters.set(SelectedAudioParametersTypes.CODEC_TYPE, Buffer.from([AudioCodecTypes.OPUS]))
  audioParameters.set(SelectedAudioParametersTypes.CODEC_PARAMETERS, TLV.encodeObject(audioCodecParameters))
  audioParameters.set(SelectedAudioParametersTypes.RTP_PARAMETERS, TLV.encodeObject(audioRtpParameters))
  audioParameters.set(SelectedAudioParametersTypes.COMFORT_NOISE, Buffer.from([0]))

  const config = new Map();
  config.set(SelectedRTPStreamConfigurationTypes.SESSION_CONTROL, TLV.encodeObject(sessionControl))
  config.set(SelectedRTPStreamConfigurationTypes.SELECTED_VIDEO_PARAMETERS, TLV.encodeObject(videoParameters))
  config.set(SelectedRTPStreamConfigurationTypes.SELECTED_AUDIO_PARAMETERS, TLV.encodeObject(audioParameters))

  //console.log('sendRTPStreamConfig', {sessionControl, videoParameters: {videoAttributes, videoRtpParameters}, audioParameters: {audioCodecParameters, audioRtpParameters}})

  const encoded = TLV.encodeObject(config)
  return await ipClient.setCharacteristics({
    '1.22': encoded.toString('base64')
  })
}

async function parseRTPStreamConfigResponse(response) {
  const { characteristics } = response
  const [characteristic] = characteristics
  const { status, value } = characteristic
  // console.log('parseRTPStreamConfigResponse', characteristic)
  if (status) {
    const error = HttpConstants.StatusCodes[status]
    throw error
  }
  if (value) {
    const buffer = Buffer.from(value, 'base64')
    const decoded = TLV.decodeBuffer(buffer)
    // console.log('parseRTPStreamConfigResponse', decoded)

    const sessionControl = TLV.decodeBuffer(decoded.get(SelectedRTPStreamConfigurationTypes.SESSION_CONTROL))
    const videoParameters = TLV.decodeBuffer(decoded.get(SelectedRTPStreamConfigurationTypes.SELECTED_VIDEO_PARAMETERS))
    const audioParameters = TLV.decodeBuffer(decoded.get(SelectedRTPStreamConfigurationTypes.SELECTED_AUDIO_PARAMETERS))

    const sessionIdentifier = sessionControl.get(SessionControlTypes.SESSION_IDENTIFIER).toString('hex')
    const command = sessionControl.get(SessionControlTypes.COMMAND).readUInt8()

    const codecType = videoParameters.get(SelectedVideoParametersTypes.CODEC_TYPE)
    const videoCodecParameters = TLV.decodeBuffer(videoParameters.get(SelectedVideoParametersTypes.CODEC_PARAMETERS))
    const videoAttributes = TLV.decodeBuffer(videoParameters.get(SelectedVideoParametersTypes.ATTRIBUTES))
    const videoRtpParameters = TLV.decodeBuffer(videoParameters.get(SelectedVideoParametersTypes.RTP_PARAMETERS))

    const audioCodecParameters = TLV.decodeBuffer(audioParameters.get(SelectedAudioParametersTypes.CODEC_PARAMETERS))
    const audioRtpParameters = TLV.decodeBuffer(audioParameters.get(SelectedAudioParametersTypes.RTP_PARAMETERS))

    // console.log('parseRTPStreamConfigResponse', {sessionIdentifier, command, videoCodecParameters, videoRtpParameters, audioCodecParameters, audioRtpParameters})
    return {sessionIdentifier, command, videoCodecParameters, videoAttributes, videoRtpParameters, audioCodecParameters, audioRtpParameters}
  }
}

async function decryptRR() {
  const setSetupEndpoint = "ARAlzOoWmfVFdK854URBERSBAxcBAQACCjEwLjAuMS4yMDADAhzjBAJh7gQlAhDeTRKsrKJ2sBGitxzYxrwfAw6940PfeF2f4bZs0yVATwEBAAUlAhA/FziKAys0s1e7R2mlltj5Aw7hdc+3CqQnoxakLFI3MwEBAA=="
  console.log('setSetupEndpoint', await parseSetupEndpoint({characteristics: [{value: setSetupEndpoint}]}))

  const getSetupEndpoint = "ARAlzOoWmfVFdK854URBERSBAgEAAxcBAQACCjEwLjAuMS4xMDgDAhzjBAJh7gQlAQEAAhCxxqHZ/JQJOyKN8jNO56TyAw6Hk1QaqxGT5aHCtBzXgwUlAQEAAhA/+sRyoYq6H10qcm53REnfAw54+mLYwbc4HnYxVq4EHAYEZnOfjAcE6E/mBA=="
  console.log('getSetupEndpoint', await parseSetupEndpoint({characteristics: [{value: getSetupEndpoint}]}))

  const setSelectedStreamConfiguration = 'ARUCAQEBECXM6haZ9UV0rznhREERFIECNAEBAAIJAQEBAgECAwEAAwsBAgAFAgLQAgMBHgQXAQFjAgT9aXhYAwIrAQQEAAAAPwUCYgUDLAEBAwIMAQEBAgEAAwEBBAEUAxYBAW4CBJUZgb0DAhgABAQAAKBABgENBAEA'
  console.log('setSelectedStreamConfiguration', await parseRTPStreamConfigResponse({characteristics: [{value: setSelectedStreamConfiguration}]}))


  const srtcpSession = new SrtcpSession({
    profile: 0x0001,
    keys: {
      localMasterKey: Buffer.from('de4d12acaca276b011a2b71cd8c6bc1f', 'hex'),
      localMasterSalt: Buffer.from('bde343df785d9fe1b66cd325404f', 'hex'),
      remoteMasterKey: Buffer.from('b1c6a1d9fc94093b228df2334ee7a4f2', 'hex'),
      remoteMasterSalt: Buffer.from('8793541aab1193e5a1c2b41cd783', 'hex'),
    }
  });

  const packet1 = Buffer.from('81c90007587869fd4a9d3d2ccda113a96e293a10e54e3f031d2e11b51dadb29f44aff76d647a007c84bfc2e9181f8fd0a949ae184725b068a5c05713aa731da37a4a90e32039eec64c9a55c0b2023cc71f741e148a748e9180000001d83a3af300bbbe4efecd', 'hex')
  const packet2 = Buffer.from('81c90007587869fda3d9ff9e0606ba8a6c6d44e112dc8dbc20285648c23b76849adf349e46d0629ca6eac1a5a241f576ca92a81045a9a92bb6128adf4920e3db1f4ded58573a29b3bd89be27a3f0f86dc916cfecf743c83180000002980ad1013b4ef2df6701', 'hex')
  const packet3 = Buffer.from('81c90007587869fd56993b55f0710e7c26278a280321d7ff8b39f7f1d668b00d5b645f08b229b1cd27aaff13b9aa548bea253e1c166e23c60c9c0957f47cc8a737b122512d3575d7beb33ad1a9f2e8c9a1538cac189bdc7380000003e527ba8b813547c570aa', 'hex')
  const packet4 = Buffer.from('81c90007587869fd8e0b85c02c1cca04a26f927c951030d431e050f2fcf860bebb304889d4aebc98142cf3391edd2499ea157c92c9b0b1610f4faf7d600f04773b9434441a7586a9241edd79f566cf9a44b7307ae395473f800000041a6dce6b91a8b21f6bf2', 'hex')
  const packet5 = Buffer.from('81c90007587869fd4170757ec1dc8b628406233dc8f4a26ae3cf3f12ceefe2d62de7ea692848c7b1a4729351f2f63ef1d26c9923bd776f2f6abea2347f5b6130b6b722e40fc377e82e0617ff254dccd7d2c8a7bb2ed6fe65800000055e25dee845e2dadca171', 'hex')
  const packet6 = Buffer.from('81c90007587869fdc48a9e5cbab3c20282ed6fa3491bea53f1265961e4ab392418690073585f828fbd42914e2a76dfceacb6e3789413241a9d5d9c556fc1e23d433a76b8ac949f60fd7308880838688ce58fae64990d93d880000006a292b74657ec2b636423', 'hex')

  /*
  console.log({
    packet1: srtcpSession.decrypt(packet1).toString('hex'),
    packet2: srtcpSession.decrypt(packet2).toString('hex'),
    packet3: srtcpSession.decrypt(packet3).toString('hex'),
    packet4: srtcpSession.decrypt(packet4).toString('hex'),
    packet5: srtcpSession.decrypt(packet5).toString('hex'),
    packet6: srtcpSession.decrypt(packet6).toString('hex'),
  })
  */

  console.log({
    enc: srtcpSession.encrypt(Buffer.from('81c90007587869fd8c9f7366000000010000000000000000000000000000000081ca0008587869fd0117726f6f7437303243463231354031302e302e312e32303000000083cd0004587869fd000000008c9f73660a47fc28', 'hex')).toString('hex'),
    packet1: packet1.toString('hex'),
  })
}
// decryptRR()

ipDiscovery.start();

