
const StreamingStatusTypes = {
  STATUS: 0x01,
}

const StreamingStatus = {
  AVAILABLE: 0x00,
  IN_USE: 0x01, // Session is marked IN_USE after the first setup request
  UNAVAILABLE: 0x02, // other reasons
}

const SupportedVideoStreamConfigurationTypes = {
  VIDEO_CODEC_CONFIGURATION: 0x01,
}

const VideoCodecConfigurationTypes = {
  CODEC_TYPE: 0x01,
  CODEC_PARAMETERS: 0x02,
  ATTRIBUTES: 0x03,
}

const VideoCodecParametersTypes = {
  PROFILE_ID: 0x01,
  LEVEL: 0x02,
  PACKETIZATION_MODE: 0x03,
  CVO_ENABLED: 0x04,
  CVO_ID: 0x05, // ID for CVO RTP extension, value in range from 1 to 14
}

const VideoAttributesTypes = {
  IMAGE_WIDTH: 0x01,
  IMAGE_HEIGHT: 0x02,
  FRAME_RATE: 0x03
}

const VideoCodecType = {
  H264: 0x00
}

const H264Profile = {
  BASELINE: 0x00,
  MAIN: 0x01,
  HIGH: 0x02,
}

const H264Level = {
  LEVEL3_1: 0x00,
  LEVEL3_2: 0x01,
  LEVEL4_0: 0x02,
}

const VideoCodecPacketizationMode = {
  NON_INTERLEAVED: 0x00
}

const VideoCodecCVO = { // Coordination of Video Orientation
  UNSUPPORTED: 0x01,
  SUPPORTED: 0x02
}

// ----------

const SupportedAudioStreamConfigurationTypes = {
  AUDIO_CODEC_CONFIGURATION: 0x01,
  COMFORT_NOISE_SUPPORT: 0x02,
}

const AudioCodecConfigurationTypes = {
  CODEC_TYPE: 0x01,
  CODEC_PARAMETERS: 0x02,
}

const AudioCodecTypes = { // only really by HAP supported codecs are AAC-ELD and OPUS
  PCMU: 0x00,
  PCMA: 0x01,
  AAC_ELD: 0x02,
  OPUS: 0x03,
  MSBC: 0x04, // mSBC is a bluetooth codec (lol)
  AMR: 0x05,
  AMR_WB: 0x06,
}

const AudioCodecParametersTypes = {
  CHANNEL: 0x01,
  BIT_RATE: 0x02,
  SAMPLE_RATE: 0x03,
  PACKET_TIME: 0x04 // only present in selected audio codec parameters tlv
}

const AudioBitrate = {
  VARIABLE: 0x00,
  CONSTANT: 0x01
}

const AudioSamplerate = {
  KHZ_8: 0x00,
  KHZ_16: 0x01,
  KHZ_24: 0x02
  // 3, 4, 5 are theoretically defined, but no idea to what kHz value they correspond to
  // probably KHZ_32, KHZ_44_1, KHZ_48 (as supported by Secure Video recordings)
}

const SupportedRTPConfigurationTypes = {
  SRTP_CRYPTO_SUITE: 0x02,
}

const SRTPCryptoSuites = { // public API
  AES_CM_128_HMAC_SHA1_80: 0x00,
  AES_CM_256_HMAC_SHA1_80: 0x01,
  NONE: 0x02
}

const SetupEndpointsTypes = {
  SESSION_ID: 0x01,
  CONTROLLER_ADDRESS: 0x03,
  VIDEO_SRTP_PARAMETERS: 0x04,
  AUDIO_SRTP_PARAMETERS: 0x05,
}

const AddressTypes = {
  ADDRESS_VERSION: 0x01,
  ADDRESS: 0x02,
  VIDEO_RTP_PORT: 0x03,
  AUDIO_RTP_PORT: 0x04,
}

const IPAddressVersion = {
  IPV4: 0x00,
  IPV6: 0x01
}

const SRTPParametersTypes = {
  SRTP_CRYPTO_SUITE: 0x01,
  MASTER_KEY: 0x02, // 16 bytes for AES_CM_128_HMAC_SHA1_80; 32 bytes for AES_256_CM_HMAC_SHA1_80
  MASTER_SALT: 0x03 // 14 bytes
}

const SetupEndpointsResponseTypes = {
  SESSION_ID: 0x01,
  STATUS: 0x02,
  ACCESSORY_ADDRESS: 0x03,
  VIDEO_SRTP_PARAMETERS: 0x04,
  AUDIO_SRTP_PARAMETERS: 0x05,
  VIDEO_SSRC: 0x06,
  AUDIO_SSRC: 0x07,
}

const SetupEndpointsStatus = {
  SUCCESS: 0x00,
  BUSY: 0x01,
  ERROR: 0x02
}


// ----------


const SelectedRTPStreamConfigurationTypes = {
  SESSION_CONTROL: 0x01,
  SELECTED_VIDEO_PARAMETERS: 0x02,
  SELECTED_AUDIO_PARAMETERS: 0x03
}

const SessionControlTypes = {
  SESSION_IDENTIFIER: 0x01, // uuid, 16 bytes
  COMMAND: 0x02,
}

const SessionControlCommand = {
  END_SESSION: 0x00,
  START_SESSION: 0x01,
  SUSPEND_SESSION: 0x02,
  RESUME_SESSION: 0x03,
  RECONFIGURE_SESSION: 0x04,
}

const SelectedVideoParametersTypes = {
  CODEC_TYPE: 0x01,
  CODEC_PARAMETERS: 0x02,
  ATTRIBUTES: 0x03,
  RTP_PARAMETERS: 0x04,
}

const VideoRTPParametersTypes = {
  PAYLOAD_TYPE: 0x01,
  SYNCHRONIZATION_SOURCE: 0x02,
  MAX_BIT_RATE: 0x03,
  MIN_RTCP_INTERVAL: 0x04, // minimum RTCP interval in seconds
  MAX_MTU: 0x05, // only there if value is not default value; default values: ipv4 1378; ipv6 1228 bytes
}

const SelectedAudioParametersTypes = {
  CODEC_TYPE: 0x01,
  CODEC_PARAMETERS: 0x02,
  RTP_PARAMETERS: 0x03,
  COMFORT_NOISE: 0x04,
}

const AudioRTPParametersTypes = {
  PAYLOAD_TYPE: 0x01,
  SYNCHRONIZATION_SOURCE: 0x02,
  MAX_BIT_RATE: 0x03,
  MIN_RTCP_INTERVAL: 0x04, // minimum RTCP interval in seconds
  COMFORT_NOISE_PAYLOAD_TYPE: 0x06
}


module.exports = {
  StreamingStatusTypes,
  StreamingStatus,
  SupportedVideoStreamConfigurationTypes,
  VideoCodecConfigurationTypes,
  VideoCodecParametersTypes,
  VideoAttributesTypes,
  VideoCodecType,
  H264Profile,
  H264Level,
  VideoCodecPacketizationMode,
  VideoCodecCVO, // Coordination of Video Orientation
  SupportedAudioStreamConfigurationTypes,
  AudioCodecConfigurationTypes,
  AudioCodecTypes, // only really by HAP supported codecs are AAC-ELD and OPUS
  AudioCodecParametersTypes,
  AudioBitrate,
  AudioSamplerate,
  SupportedRTPConfigurationTypes,
  SRTPCryptoSuites, // public API
  SetupEndpointsTypes,
  AddressTypes,
  IPAddressVersion,
  SRTPParametersTypes,
  SetupEndpointsResponseTypes,
  SetupEndpointsStatus,
  SelectedRTPStreamConfigurationTypes,
  SessionControlTypes,
  SessionControlCommand,
  SelectedVideoParametersTypes,
  VideoRTPParametersTypes,
  SelectedAudioParametersTypes,
  AudioRTPParametersTypes,
};
